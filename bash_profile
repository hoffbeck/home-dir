# Get Real script base path
BASE_PATH="$(dirname "$(readlink -f "$0")")"

# Load all bash dot files
[[ ! -f "${BASE_PATH}/paths" ]] || source ${BASE_PATH}/paths