# Get Real script base path
BASE_PATH="$(dirname "$(readlink -f "${HOME}/.zshrc")")"

# Load all bash dot files
[[ ! -f "${BASE_PATH}/paths" ]] || source ${BASE_PATH}/paths

# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

# Load all bash dot files
#[[ ! -f ~/.bash_profile ]] || source ~/.bash_profile

# Load antigen
source ~/.antigenrc

# Load custom aliases
[[ -s "$HOME/".pam_environment ]] && source "$HOME/".pam_environment

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh

# Load gcloud (Homebrew)
[[ -s "/opt/homebrew/share/google-cloud-sdk/completion.zsh.inc" ]] && source "/opt/homebrew/share/google-cloud-sdk/completion.zsh.inc"
autoload -U compinit
compinit

# Load custom loaders
[[ ! -f "${BASE_PATH}/custom" ]] || source ${BASE_PATH}/custom